Prudential DS installation
Ansible playbook to Install prerequisties, downloads the binary,install userstore,idmstore,ctsstore and configstore in respective servers. 

Variables that can be passed dynamically during playbook execution
root_user_password: "Password123"
monitor_user_password: "Password123"
keystore_password: changeit
am_identitystore_admin_password: "Password123"
am_configstore_admin_password: "Password123"
am_ctsstore_admin_password: "Password123"

Running Playbook Example
ansible-playbook -i inventories install.yml 

Inventory
Update hostnames in inventories/hosts file. Example:

[userstore]
104.43.14.109

[idmstore]
23.97.49.174

[ctsstore]
13.76.38.158

[configstore]
52.187.109.223


Roles:
Below are the roles used in this playbook
userstore_installl -- Download and Untars the binary and install userstore
idmstore_installl -- Download and Untars the binary and install idmstore
ctsstore_install -- Download and Untars the binary and install ctsstore
configstore_install -- Download and Untars the binary and install configstore


Run the following playbook to uninstall userstore, idmstore, ctsstore and configstore in respective servers
-------------------------------------------------
### uninstall.yml -- Uninstalls Java, userstore, idmstore, ctsstore and configstore in respective servers.

ansible-playbook -i inventories uninstall.yml

